# awesome-codeday

Links and resources for [CodeDay](https://www.codeday.org/) attendees and mentors.

This repo was started and is currently maintained by [CodeDay Phoenix](https://www.codeday.org/phoenix).

# Simple Game Making Platforms

* [Scratch](https://scratch.mit.edu/)
* [Snap! (Build Your Own Blocks)](https://snap.berkeley.edu/)
* [Unity Playground](https://unity3d.com/learn/tutorials/s/unity-playground)

# Complex Game Making Platforms

* [Construct 3](https://www.scirra.com/)
* [GameMaker](https://www.yoyogames.com/gamemaker)
* [gDevelop](https://gdevelop-app.com/)
* [GoDot](https://godotengine.org/)
* [Phaser](http://phaser.io/)
* [PICO-8](https://www.lexaloffle.com/pico-8.php)
* [Unity](https://unity3d.com/)

## Open Source Games

* [Open Source Game Clones](https://osgameclones.com/) ([Git Repo](https://github.com/opengaming/osgameclones/)): Open Source Clones of Popular Games
* [awesome-game-remakes](https://github.com/radek-sprta/awesome-game-remakes): Actively maintained open-source game remakes.

# Collaboration Tools

* [Gitlab](https://www.gitlab.com)
* [Github](https://www.github.com)

# Text Editors

* [Visual Studio Code](https://code.visualstudio.com/)

# Useful Videos

* [Boundary Break](https://www.youtube.com/user/PencakeAndWuffle) - Details on how video games are made by breaking what you normally see in video games.

# Art Work

* [Open Game Art](https://opengameart.org/)

# Documentation

* [explainshell.com](https://explainshell.com/) - match command-line arguments to their help text
* [DevDocs API Documentation](https://devdocs.io/) - easily searchable docs for a lot of languages

# Image/Model Editting

* [Blender 3D](https://www.blender.org/)
* [GIMP](https://www.gimp.org/)

# Additional Visual Programming Environments

## Environments

* [Alice – Tell Stories. Build Games. Learn to Program.](https://www.alice.org/)
* [WayScript](https://www.wayscript.com/)
* [cables](https://cables.gl/)
* [Visual Programming | Bubble](https://bubble.io/)
* [vvvv - a multipurpose toolkit | vvvv](https://vvvv.org/)
* [Luna](https://www.luna-lang.org/)
* [XOD](https://xod.io/)
* [GB Studio](https://www.gbstudio.dev/)
* [Kodu - Microsoft Research](https://www.microsoft.com/en-us/research/project/kodu/)
* [PraxisLIVE](https://www.praxislive.org/)

## Articles

* [Interactive Visual Programming With Vvvv | Hackaday](https://hackaday.com/2017/10/26/interactive-visual-programming-with-vvvv/)
* [Why Visual Programming Doesn’t Suck - Statebox](https://blog.statebox.org/why-visual-programming-doesnt-suck-2c1ece2a414e)

# More Awesome Resources

* [aviaryan/awesome-no-login-web-apps: 🚀 Awesome (free) web apps that work without login](https://github.com/aviaryan/awesome-no-login-web-apps#readme)
* [LisaDziuba/Awesome-Design-Tools: The best design tools and plugins for everything 👉](https://github.com/LisaDziuba/Awesome-Design-Tools)
* [sindresorhus/awesome: 😎 Awesome lists about all kinds of interesting topics](https://github.com/sindresorhus/awesome)
* [GitHub Topic: awesome-list](https://github.com/topics/awesome-list)
